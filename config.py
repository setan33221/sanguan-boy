import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())
REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = os.getenv('REDIS_PORT')
REDIS_PREFIX = os.getenv('REDIS_PREFIX')
CORS_ALLOWED_ORIGINS = os.getenv('CORS_ALLOWED_ORIGINS')
GO_STREAM_SECRET = os.getenv('GO_STREAM_SECRET')