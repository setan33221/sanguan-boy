import redis
import subprocess
from config import REDIS_HOST, REDIS_PORT, REDIS_PREFIX


class RedisClient:
    def __init__(self, subPrefix):
        self.subPrefix = subPrefix
        self.redis = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)

    def get(self, key):
        key = REDIS_PREFIX + ":" + self.subPrefix + ':' + key
        return self.redis.get(name=key)

    def set(self, key, value):
        key = REDIS_PREFIX + ":" + self.subPrefix + ':' + key
        return self.redis.set(name=key, value=value)

    def delete(self, key):
        key = REDIS_PREFIX + ":" + self.subPrefix + ':' + key
        return self.redis.delete(key)

    def countStoredRedisKeyByPattern(self, pattern):
        count = len(self.redis.keys(pattern))
        return count
