#!/bin/bash
source /usr/local/bin/virtualenvwrapper.sh
cd /data/application/loket-live-guard
workon live-guard
exec python3 main.py config.ini
