from autobahn.twisted.websocket import WebSocketServerProtocol, \
    WebSocketServerFactory
from config import CORS_ALLOWED_ORIGINS
from client import RedisClient
import json
import jwt
import base64
import phpserialize
redisClientSession = RedisClient(subPrefix="SESSION")
redisClientEvent = RedisClient(subPrefix="EVENT")


class LiveGuardFactory(WebSocketServerFactory):
    def __init__(self, url):
        WebSocketServerFactory.__init__(self, url)
        self.clients = []

    def register(self, client, room):
        if client not in self.clients:
            self.clients.append({"client": client, "room": room})

    def unregister(self, client, room):
        for c in filter(lambda x: x['client'] == client, self.clients):
            splittedRoom = str(base64.b64decode(c['room']), 'utf-8').split("|")
            payload = {
                'event_id': splittedRoom[0],
                'voucher_code': splittedRoom[2],
                'barcode_id': splittedRoom[1],
            }
            tokenToRemove = str(payload['event_id']) + \
                ":" + self.generateLiveGuardToken(payload)
            redisClientSession.delete(tokenToRemove)
            self.clients.remove(c)

    def broadcast(self, msg, room='*'):
        if(room == '*'):
            for c in self.clients:
                c["client"].sendMessage(json.dumps(msg).encode('utf8'))
        else:
            for c in filter(lambda x: x['room'] == room, self.clients):
                c["client"].sendMessage(json.dumps(msg).encode('utf8'))

    def generateLiveGuardToken(self, payload):
        tokenLiveGuard = base64.b64encode(((payload['event_id']) + "|" +
                                           (payload['barcode_id']) + "|" + (payload['voucher_code'])).encode('utf-8')).decode('utf-8')
        return tokenLiveGuard


class LiveGuardServerProtocol(WebSocketServerProtocol):
    TYPE_CHECK_EVENT = 'check_event'
    TYPE_JOIN_CHAT_ROOM = 'join_chat_room'
    TYPE_CONNECT_TO_LIVE_EVENT = 'connect_to_live_event'
    TYPE_DISCONNECT_TO_LIVE_EVENT = 'disconnect_to_live_event'
    TYPE_EVENT_NOT_STARTED = 'event_not_started'
    TYPE_EVENT_STARTED = 'event_started'
    TYPE_GRANTED = 'granted'
    TYPE_KICKED = 'kicked'
    TYPE_CHECK_SESSION = 'count_connected_session'

    def onConnect(self, request):
        payload = {
            'event_id': str(request.params['event_id'][0]),
            'voucher_code': str(request.params['voucher_code'][0]),
            'barcode_id': str(request.params['barcode_id'][0]),
        }
        factory.register(self, room=factory.generateLiveGuardToken(payload))
        print("Client connecting: {0}".format(request.peer))

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary,):
        payload = json.loads(str(payload, 'utf-8'))
        liveStreamingData = redisClientEvent.get(key=str(payload['event_id']))
        if(liveStreamingData != None):
            liveStreamingData = phpserialize.loads(liveStreamingData)
            print(liveStreamingData[b'chat_room']
                  [b'secret_key'], payload['token'])
            if(str(liveStreamingData[b'chat_room'][b'secret_key'], 'utf-8') != payload['token']):
                return factory.broadcast({
                    'action_type': 'invalid_token',
                    'session': payload['session'],
                }, room=factory.generateLiveGuardToken(payload))
        if(payload['action_type'] == self.TYPE_CHECK_EVENT):
            self._handleCheckEvent(payload)
        elif(payload['action_type'] == self.TYPE_JOIN_CHAT_ROOM):
            self._handleJoinChatRoom(payload)
        elif(payload['action_type'] == self.TYPE_CONNECT_TO_LIVE_EVENT):
            self._handleConnectedToLiveEvent(payload)
        elif(payload['action_type'] == self.TYPE_DISCONNECT_TO_LIVE_EVENT):
            self._handleDisConnectedToLiveEvent(payload)
        elif(payload['action_type'] == self.TYPE_CHECK_SESSION):
            self._handleCheckSession(payload)

    def onClose(self, wasClean, code, reason):
        factory.unregister(self, "*")

    def _handleCheckEvent(self, payload):
        liveStreamingData = redisClientEvent.get(key=str(payload['event_id']))
        if(liveStreamingData == None):
            factory.broadcast({
                'action_type': self.TYPE_EVENT_NOT_STARTED,
                'eventId': payload['event_id'],
            }, room=factory.generateLiveGuardToken(payload))
        else:
            factory.broadcast({
                'action_type': self.TYPE_EVENT_STARTED,
                'eventId': payload['event_id']
            }, room=factory.generateLiveGuardToken(payload))

    def _handleJoinChatRoom(self, payload):
        key = str(payload['event_id']) + ":" + \
            "USERNAME" + ":" + payload['session']
        if(not payload['recon']):
            redisClientSession.delete(key=key)
        token = redisClientSession.get(key=key)
        if(token == None):
            liveStreamingpayload = redisClientEvent.get(
                key=str(payload['event_id']))
            liveStreamingpayload = phpserialize.loads(liveStreamingpayload)
            secretKey = str(
                liveStreamingpayload[b'chat_room'][b'secret_key'], 'utf-8')
            token = jwt.encode({
                'event': (payload['event_id']),
                'name': payload['username'],
                'iat': int(payload['iat'])
            }, secretKey, algorithm='HS256')
            token = str(token, 'utf-8')
            redisClientSession.set(key=key, value=token)
        else:
            token = token.decode('utf-8')

        factory.broadcast({
            'action_type': 'join_chat_success',
            'session': payload['session'],
            'recon': payload['recon'],
            'token': token,
        }, room=factory.generateLiveGuardToken(payload))

    def _handleConnectedToLiveEvent(self, payload):
        liveStreamingData = redisClientEvent.get(key=str(payload['event_id']))
        liveStreamingData = phpserialize.loads(liveStreamingData)
        key = factory.generateLiveGuardToken(payload)
        key = str(payload['event_id']) + ":" + key
        currentUser = redisClientSession.get(key=key)
        if(currentUser == None):
            currentUser = ''
        else:
            currentUser = currentUser.decode('utf-8')

        if(currentUser == ''):
            redisClientSession.set(key=key, value=payload['session'])
            factory.broadcast({
                'action_type': 'granted',
                'session': payload['session'],
                'playback_url': str(liveStreamingData[b'player_hls_playback_url'], 'utf-8'),
            }, room=factory.generateLiveGuardToken(payload))
        elif(currentUser != payload['session']):  # 2nd kicked
            factory.broadcast({
                'action_type': self.TYPE_KICKED,
                'session': payload['session'],
            }, room=factory.generateLiveGuardToken(payload))
        else:
            factory.broadcast({
                'action_type': self.TYPE_GRANTED,
                'session': currentUser, 'playback_url':
                str(liveStreamingData[b'player_hls_playback_url'], 'utf-8'),
            }, room=factory.generateLiveGuardToken(payload))
        self._handleCheckSession(payload)

    def _handleCheckSession(self, payload):
        redisClientSession = RedisClient(subPrefix="SESSION")
        factory.broadcast({
            'action_type': 'connected_session',
            'event_id': payload['event_id'],
            'active': redisClientSession.countStoredRedisKeyByPattern(
                pattern="LIVE_GUARD:SESSION:"+str(payload['event_id']+":*"))
        })


if __name__ == '__main__':
    import sys

    from twisted.python import log
    from twisted.internet import reactor

    log.startLogging(sys.stdout)

    factory = LiveGuardFactory("ws://127.0.0.1:2000")
    factory.protocol = LiveGuardServerProtocol
    factory.allowedOrigins = ['*']
    reactor.listenTCP(2000, factory)
    reactor.run()
