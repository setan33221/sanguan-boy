# Installation
```
pip install -r requirements.txt
```
# How to run it
```
python main.py
```
# Information
## Redis Key
- LIVE_GUARD:SESSION:{idEvennt}:base64({barcode_id+voucher_code})
- LIVE_GUARD:EVENT:{idEvent}
## Available Event
- disconected_to_live_event
- connected_to_live_event
- event_not_started
- kicked (will return kicked session id)
- granted (will return granted session id)
- event_started
- join_chat_room
- join_chat_success